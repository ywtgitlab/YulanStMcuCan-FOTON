/* Includes ------------------------------------------------------------------*/
#include "appUart.h"
#include <string.h>
#include "user_hal_common.h"
#include "main.h"    

/* Private typedef -----------------------------------------------------------*/
UART_FrameTypeDef uFrame;

unsigned char u123TxBuffer[10];

static __u8 UART_DataRccVerify(unsigned char *data,unsigned int size);

void UART_MegTxPro(UART_FrameTypeDef frame)
{
	int i;
//        if (0 == memcmp(&uFramePre, &uFrame, sizeof(UART_FrameTypeDef))) return;	
//           
//        memcpy(&uFramePre, &uFrame, sizeof(UART_FrameTypeDef));

    frame.start_bit = HEAD_FRAME;
	frame.stop_bit = STOP_FRAME;
        
	u123TxBuffer[0] = frame.start_bit;
	u123TxBuffer[1] = frame.uart_id;
	u123TxBuffer[2] = frame.uart_rw ;
	u123TxBuffer[3] = frame.uart_dlc;
	for(i = 0; i < frame.uart_dlc; i++)
	{
		u123TxBuffer[4+i] = frame.data[i];
	}
	u123TxBuffer[CMD_FRAME_SIZE + frame.uart_dlc - 2] = UART_DataRccVerify(u123TxBuffer,VERIFY_FRAME_SIZE+frame.uart_dlc);
	u123TxBuffer[CMD_FRAME_SIZE + frame.uart_dlc - 1] = frame.stop_bit;
    SET_DATE_Transmit_IT(u123TxBuffer, CMD_FRAME_SIZE + frame.uart_dlc);
}
/**
  * @}
  */
static __u8 UART_DataRccVerify(unsigned char *data,unsigned int size)
{
	__u8 retVerify;
	int i;
	retVerify = data[0];
	for(i = 1; i <= size; i++)
	{
			retVerify = retVerify^data[i];
	}
	return retVerify;
}
