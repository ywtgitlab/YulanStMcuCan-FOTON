#include "main.h"
#include "appCommon.h"
#include "foton-can.h"
#include "user_hal_common.h"
#include <string.h>


CAN_FrameTypeDef CanRxBuf;
CAN_FrameTypeDef CanTxBuf;
UART_FrameTypeDef DataTxBuf;

CycleMsg_t  CycleMsgFoton = {
    .AVM1_St_AVM_Mode = ST_AVM_MODE_INACTIVE,
    .AVM1_St_Calibration = 0,
    .AVM1_St_ViewInd = ST_VIEWIND_FRONT,
    .AVM1_St_AVMOn = ST_ON_AVM_NORMAL,
    .AVM1_St_Warn_AVM = ST_WARN_NONE,
    .AVM1_F_FrontCamera = 1,
    .AVM1_F_RearCamera = 1,
    .AVM1_F_LeftCamera = 1,
    .AVM1_F_RightCamera = 1
};

/* user private varibal */
Audio2Msg_t     audio2_msg_foton;
Icm1Msg_t       icm1_msg_foton;
Bcm5Msg_t       bcm5_msg_foton;
SAS1Msg_t       sas1_msg_foton;

extern CAN_HandleTypeDef    CanHandle;
volatile uint8_t g_need_check_can = 0;

static void Icm1_Switch_Uart(CAN_FrameTypeDef *can_frame);
static void Audio2_Switch_Uart(CAN_FrameTypeDef *can_frame);
static void Bcm5_Switch_Uart(CAN_FrameTypeDef *can_frame);
static void SAS1_Switch_Uart(CAN_FrameTypeDef *can_frame);


void CAN_RxMegAnalysis(CAN_FrameTypeDef *can_frame)
{
    switch(can_frame->can_id)
    {
    case CAN_ID_BRAKE1: //车速
        //frame.data
        
        break;
    case CAN_ID_SAS1:   //方向盘转角
        //pear 23H means 1°
        SAS1_Switch_Uart(can_frame);
        
        break;
    case CAN_ID_BCM5:   //转向开关
        g_need_check_can = 1;
        if (icm1_msg_foton.ICM1_N_ReverseGear == 0x1) return;
        Bcm5_Switch_Uart(can_frame);
        break;
    case CAN_ID_ICM1:   //挡位
        g_need_check_can = 1;
        Icm1_Switch_Uart(can_frame);
        break;
    case CAN_ID_AUDIO2: //视图
        if (icm1_msg_foton.ICM1_N_ReverseGear == 0x1) return;
        Audio2_Switch_Uart(can_frame);
        break;
    }
}
  
static void SAS1_Switch_Uart(CAN_FrameTypeDef *can_frame)   
{
    static uint8_t temp_datacmd = 0;
    static uint8_t switch_s_direction = 0;
    
    memcpy(&sas1_msg_foton, can_frame->data, 8);
    
    if (temp_datacmd == sas1_msg_foton.EPS3_N_CSWA && switch_s_direction == sas1_msg_foton.EPS3_St_CSWASign)
        return;
    
    temp_datacmd = sas1_msg_foton.EPS3_N_CSWA/0x23;
    switch_s_direction = sas1_msg_foton.EPS3_St_CSWASign;
    
    DataTxBuf.uart_id   = 0x01;
    DataTxBuf.uart_rw   = 0x02;
    DataTxBuf.uart_dlc  = 0x02;
    DataTxBuf.data[0]   = switch_s_direction;
    DataTxBuf.data[1]   = temp_datacmd;
    UART_MegTxPro(DataTxBuf);    
}

static void Bcm5_Switch_Uart(CAN_FrameTypeDef *can_frame)   
{
    static uint8_t temp_datacmd = 0;
    static uint8_t switch_s_direction = 0;
    
    if (0 == memcmp(&bcm5_msg_foton, can_frame->data, 8)) return;
    memcpy(&bcm5_msg_foton, can_frame->data, 8);
    
    if (bcm5_msg_foton.LA1_S_DirectionLampL == 0x0 && bcm5_msg_foton.LA1_S_DirectionLampR == 1){
        temp_datacmd = ST_VIEWIND_RIGHT;
        switch_s_direction = 1;
        CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_RIGHT;
    }else if (bcm5_msg_foton.LA1_S_DirectionLampL == 0x1 && bcm5_msg_foton.LA1_S_DirectionLampR == 0){
        temp_datacmd = ST_VIEWIND_LEFT;
        switch_s_direction = 1;
        CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_LEFT;
    }else if (bcm5_msg_foton.LA1_S_DirectionLampL == 0 && bcm5_msg_foton.LA1_S_DirectionLampR == 0 && switch_s_direction){
        temp_datacmd = ST_VIEWIND_FRONT;
        switch_s_direction = 0;
        CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
    }else {
        return ;
    }
    
    DataTxBuf.uart_id   = 0x01;
    DataTxBuf.uart_rw   = 0x02;
    DataTxBuf.uart_dlc  = 0x01;
    DataTxBuf.data[0]   = temp_datacmd;
    UART_MegTxPro(DataTxBuf);    
}

static void Icm1_Switch_Uart(CAN_FrameTypeDef *can_frame)
{
    uint8_t         temp_datacmd = 0;
    static uint8_t  viewind_last = 0;
    
    if (0 == memcmp(&icm1_msg_foton, can_frame->data, 8)) return;
    memcpy(&icm1_msg_foton, can_frame->data, 8);
    
    if (icm1_msg_foton.ICM1_N_ReverseGear == 0x1){
        temp_datacmd = ST_VIEWIND_REAR;
        viewind_last = CycleMsgFoton.AVM1_St_ViewInd;
        CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_REAR;
        CycleMsgFoton.AVM1_St_AVMOn = ST_ON_AVM_NORMAL;        
    }else if(icm1_msg_foton.ICM1_N_ReverseGear == 0x0){
        temp_datacmd = viewind_last;
        CycleMsgFoton.AVM1_St_ViewInd = viewind_last;
        //CycleMsgFoton.AVM1_St_AVMOn = ST_ON_AVM_OFF;
    }
    
    DataTxBuf.uart_id   = 0x01;
    DataTxBuf.uart_rw   = 0x02;
    DataTxBuf.uart_dlc  = 0x01;
    DataTxBuf.data[0]   = temp_datacmd;
    UART_MegTxPro(DataTxBuf);
}
                                                     
static void Audio2_Switch_Uart(CAN_FrameTypeDef *can_frame)   
{    
    static uint8_t temp_datacmd = 0;
    static uint8_t temp_bright = 10;
    static uint8_t temp_2d3d = 0x1;
    memcpy(&audio2_msg_foton, can_frame->data, 8);
    
    if (audio2_msg_foton.AUDIO2_S_AVM == 0x2 
        && audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x0 
            && audio2_msg_foton.AUDIO2_S_AVM_Mode == 0x0 
                && audio2_msg_foton.AUDIO2_S_Brightnesscontrol == 0) 
    {
        temp_datacmd = ST_VIEWIND_FRONT;
        CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
        CycleMsgFoton.AVM1_St_AVMOn = ST_ON_AVM_NORMAL;
        goto data_tx_ready;
    }else if (audio2_msg_foton.AUDIO2_S_AVM == 0x1) {
        temp_datacmd = ST_VIEWIND_FRONT;
        CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
        CycleMsgFoton.AVM1_St_AVMOn = ST_ON_AVM_OFF;
        goto data_tx_ready;
        //return ;
    }
    
    if (audio2_msg_foton.AUDIO2_S_AVM == 0x2 && audio2_msg_foton.AUDIO2_S_Brightnesscontrol != 0){
        
        if (audio2_msg_foton.AUDIO2_S_Brightnesscontrol == 0x1){ //up
            temp_bright++;
        }else if (audio2_msg_foton.AUDIO2_S_Brightnesscontrol == 0x2){ //down
            temp_bright--;
        }
        
        DataTxBuf.uart_id   = 0x05;
        DataTxBuf.uart_rw   = 0x02;
        DataTxBuf.uart_dlc  = 0x03;
        DataTxBuf.data[0]   = 0x01;
        DataTxBuf.data[1]   = 0x01;
        DataTxBuf.data[2]   = temp_bright;
        UART_MegTxPro(DataTxBuf);
        
        return ;
    }
    
    if (audio2_msg_foton.AUDIO2_S_AVM_Mode == 0x1 || audio2_msg_foton.AUDIO2_S_AVM_Mode == 0x2){
        temp_2d3d = audio2_msg_foton.AUDIO2_S_AVM_Mode;
    }
    
    if (temp_2d3d == 0x1){
        CycleMsgFoton.AVM1_St_AVM_Mode = ST_AVM_MODE_2D;
        
        if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x1){          //front
            temp_datacmd = ST_VIEWIND_FRONT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x2 || audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x5){    //rear | ReverseGear
            temp_datacmd = ST_VIEWIND_REAR;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_REAR;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x3){    //left
            temp_datacmd = ST_VIEWIND_LEFT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_LEFT;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x4){    //right
            temp_datacmd = ST_VIEWIND_RIGHT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_RIGHT;
        }else {
            temp_datacmd = ST_VIEWIND_FRONT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
        }
    }else if (temp_2d3d == 0x2){
        CycleMsgFoton.AVM1_St_AVM_Mode = ST_AVM_MODE_3D;

        if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x1){          //front
            temp_datacmd = ST_VIEWIND_3D_BASE + ST_VIEWIND_FRONT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x2){    //rear
            temp_datacmd = ST_VIEWIND_3D_BASE + ST_VIEWIND_REAR;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_REAR;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x5){    //ReverseGear
            temp_datacmd = ST_VIEWIND_REAR;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_REAR;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x3){    //left
            temp_datacmd = ST_VIEWIND_3D_BASE + ST_VIEWIND_LEFT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_LEFT;
        }else if (audio2_msg_foton.AUDIO2_St_AVMViewSel == 0x4){    //right
            temp_datacmd = ST_VIEWIND_3D_BASE + ST_VIEWIND_RIGHT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_RIGHT;
        }else {
            temp_datacmd = ST_VIEWIND_3D_BASE + ST_VIEWIND_FRONT;
            CycleMsgFoton.AVM1_St_ViewInd = ST_VIEWIND_FRONT;
        }
    }

data_tx_ready:
    
    DataTxBuf.uart_id   = 0x01;
    DataTxBuf.uart_rw   = 0x02;
    DataTxBuf.uart_dlc  = 0x01;
    DataTxBuf.data[0]   = temp_datacmd;
    UART_MegTxPro(DataTxBuf);
    
}

void CAN_CycleMegTx(void)
{
    static uint32_t timeStart = 0;
    
    if((HAL_GetTick() - timeStart) < 500) return;
    timeStart = HAL_GetTick();
    
    Foton_CanMegTx(&CycleMsgFoton);    
}

void Foton_CanMegTx(CycleMsg_t *data_Frame)
{

		CanHandle.pTxMsg->StdId = 0x477;
		CanHandle.pTxMsg->ExtId = 0x01;
		CanHandle.pTxMsg->RTR = CAN_RTR_DATA;
		CanHandle.pTxMsg->IDE = CAN_ID_STD;
		CanHandle.pTxMsg->DLC = 8;	

		memcpy(CanHandle.pTxMsg->Data, data_Frame, 8);
        
        SET_CAN_Transmit_IT(&CanHandle);
}
