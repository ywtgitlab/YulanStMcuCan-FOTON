#include <stdbool.h>
#include <string.h>

#include "main.h"
#include "user_hal_common.h"
#include "appCommon.h"
#include "foton-can.h"

CAN_HandleTypeDef    CanHandle;


static void CAN_Config(uint32_t baudrate_kbps);
static void Can_Error_Handler(void);

CAN_FrameTypeDef can_rx_irq_frame;
volatile uint32_t g_can_rx_irq_count;
volatile uint32_t can_err_irq_count;
volatile int32_t g_can_err_stat_count;
volatile bool g_CanRxMeg_Ready;
volatile bool g_CanHal_Error;

void CAN_Init(uint32_t baudrate_kbps)
{

    /*##-1- Configure the CAN peripheral #######################################*/
    CAN_Config(baudrate_kbps);
    
    /*##-2- Start the Reception process and enable reception interrupt #########*/
    if (HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0) != HAL_OK)
    {
        /* Reception Error */
        Can_Error_Handler();
    }

    //sysVariate.canConnectState = 1; 
}

/**
  * @brief  Configures the CAN.
  * @param  None
  * @retval None
  */
static void CAN_Config(uint32_t baudrate_kbps)
{
  CAN_FilterConfTypeDef  sFilterConfig;
  static CanTxMsgTypeDef        TxMessage;
  static CanRxMsgTypeDef        RxMessage;
  uint32_t slave_id;
  
  /*##-1- Configure the CAN peripheral #######################################*/
  CanHandle.Instance = CANx;
  CanHandle.pTxMsg = &TxMessage;
  CanHandle.pRxMsg = &RxMessage;

  CanHandle.Init.TTCM = DISABLE;
  CanHandle.Init.ABOM = DISABLE;
  CanHandle.Init.AWUM = DISABLE;
  CanHandle.Init.NART = DISABLE;
  CanHandle.Init.RFLM = DISABLE;
  CanHandle.Init.TXFP = DISABLE;
  CanHandle.Init.Mode = CAN_MODE_NORMAL;
  CanHandle.Init.SJW = CAN_SJW_1TQ;
  CanHandle.Init.BS1 = CAN_BS1_15TQ;    //default:CAN_BS1_5TQ
  CanHandle.Init.BS2 = CAN_BS2_8TQ;     //default:CAN_BS2_6TQ
  if (500 == baudrate_kbps){
    CanHandle.Init.Prescaler = 4;
  }else if (250 == baudrate_kbps){
    CanHandle.Init.Prescaler = 8;
  }else if (125 == baudrate_kbps){
    CanHandle.Init.Prescaler = 16;
  }else if (100 == baudrate_kbps){
    CanHandle.Init.Prescaler = 20;
  }else{
    /* can bit rate set error */
    while(1);
  }
  
  if (HAL_CAN_Init(&CanHandle) != HAL_OK)
  {
    /* Initialization Error */
    Can_Error_Handler();
  }

  /*##-2- Configure the CAN Filter ###########################################*/
//  slave_id = 0x17330B00;
//  sFilterConfig.FilterNumber = 0;
//  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
//  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
//  sFilterConfig.FilterIdHigh = (((uint32_t)slave_id<<3)&0xFFFF0000)>>16;//0xB998;//0x92e0;//0x9098;//0x0098;
//  sFilterConfig.FilterIdLow = (((uint32_t)slave_id<<3)|CAN_ID_EXT|CAN_RTR_DATA)&0xFFFF;//0x5800;//0x0000;//0x5800;
//  sFilterConfig.FilterMaskIdHigh = 0xffff;//0xffff;//0xd49f;//0x409f;
//  sFilterConfig.FilterMaskIdLow = 0xffff;//0xffff;//0xfffa;
//  sFilterConfig.FilterFIFOAssignment = 0;
//  sFilterConfig.FilterActivation = ENABLE;
//  sFilterConfig.BankNumber = 14;
//
//  if (HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
//  {
//    /* Filter configuration Error */
//    Can_Error_Handler();
//  }
  
  slave_id = 0x344;
  sFilterConfig.FilterNumber = 1;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = (((uint32_t)slave_id<<21)&0xffff0000)>>16;//0x92e0;//0x92e0;//0x9098;//0x0098;
  sFilterConfig.FilterIdLow = (((uint32_t)slave_id<<21)|CAN_ID_STD|CAN_RTR_DATA)&0xffff;//0x0000;//0x0000;//0x5800;
  sFilterConfig.FilterMaskIdHigh = 0xffff;//0xffff;//0xd49f;//0x409f;
  sFilterConfig.FilterMaskIdLow = 0xffff;//0xffff;//0xfffa;
  sFilterConfig.FilterFIFOAssignment = 0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.BankNumber = 14;

  if (HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
  {
    /* Filter configuration Error */
    Can_Error_Handler();
  }
  
  slave_id = 0x03a;
  sFilterConfig.FilterNumber = 2;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = (((uint32_t)slave_id<<21)&0xffff0000)>>16;//0x92e0;//0x92e0;//0x9098;//0x0098;
  sFilterConfig.FilterIdLow = (((uint32_t)slave_id<<21)|CAN_ID_STD|CAN_RTR_DATA)&0xffff;//0x0000;//0x0000;//0x5800;
  sFilterConfig.FilterMaskIdHigh = 0xffff;//0xffff;//0xd49f;//0x409f;
  sFilterConfig.FilterMaskIdLow = 0xffff;//0xffff;//0xfffa;
  sFilterConfig.FilterFIFOAssignment = 0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.BankNumber = 14;

  if (HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
  {
    /* Filter configuration Error */
    Can_Error_Handler();
  } 
  
//  slave_id = 0x470;
//  sFilterConfig.FilterNumber = 3;
//  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
//  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
//  sFilterConfig.FilterIdHigh = (((uint32_t)slave_id<<21)&0xffff0000)>>16;//0x92e0;//0x92e0;//0x9098;//0x0098;
//  sFilterConfig.FilterIdLow = (((uint32_t)slave_id<<21)|CAN_ID_STD|CAN_RTR_DATA)&0xffff;//0x0000;//0x0000;//0x5800;
//  sFilterConfig.FilterMaskIdHigh = 0xffff;//0xffff;//0xd49f;//0x409f;
//  sFilterConfig.FilterMaskIdLow = 0xffff;//0xffff;//0xfffa;
//  sFilterConfig.FilterFIFOAssignment = 0;
//  sFilterConfig.FilterActivation = ENABLE;
//  sFilterConfig.BankNumber = 14;
//
//  if (HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
//  {
//    /* Filter configuration Error */
//    Can_Error_Handler();
//  }   
//  
//  slave_id = 0xAD;
//  sFilterConfig.FilterNumber = 4;
//  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
//  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
//  sFilterConfig.FilterIdHigh = (((uint32_t)slave_id<<21)&0xffff0000)>>16;//0x92e0;//0x92e0;//0x9098;//0x0098;
//  sFilterConfig.FilterIdLow = (((uint32_t)slave_id<<21)|CAN_ID_STD|CAN_RTR_DATA)&0xffff;//0x0000;//0x0000;//0x5800;
//  sFilterConfig.FilterMaskIdHigh = 0xffff;//0xffff;//0xd49f;//0x409f;
//  sFilterConfig.FilterMaskIdLow = 0xffff;//0xffff;//0xfffa;
//  sFilterConfig.FilterFIFOAssignment = 0;
//  sFilterConfig.FilterActivation = ENABLE;
//  sFilterConfig.BankNumber = 14;
//
//  if (HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
//  {
//    /* Filter configuration Error */
//    Can_Error_Handler();
//  }     
  
}

void CHECK_CAN_RxItCount(void)
{
    static uint32_t CanRxIt_lastCount = 0;
    static uint32_t Func200ms_Count = 0;
    
    if (10 >= ++Func200ms_Count) return ;
    
    Func200ms_Count = 0;
    
    if (g_can_rx_irq_count - CanRxIt_lastCount){
        CanRxIt_lastCount = g_can_rx_irq_count;
    }else{
        HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0);
    } 
}

HAL_StatusTypeDef SET_CAN_Receive_IT(CAN_HandleTypeDef* hcan, uint8_t FIFONumber)
{
    HAL_StatusTypeDef   result;
    result = HAL_CAN_Receive_IT(hcan, CAN_FIFO0);
    if (result != HAL_OK)
    {
        /* Reception Error */
        Can_Error_Handler();
    }
    return result;
}
       
HAL_StatusTypeDef SET_CAN_Transmit_IT(CAN_HandleTypeDef *hcan)
{
    HAL_StatusTypeDef   result;
    result = HAL_CAN_Transmit_IT(hcan);
    if (result != HAL_OK)
    {
        /* Reception Error */
        //Can_Error_Handler();
    }
    return result;
}

void SET_CAN_Force_Rx_IT(void)
{
    uint32_t    err_delay = 0x7ffff;
    HAL_StatusTypeDef   result;
    while(--err_delay);

    if (HAL_CAN_ERROR_NONE == HAL_CAN_GetError(&CanHandle))
    {
        /* if error ocured in this step, how to do it? */
        result = HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0);
        if (result == HAL_OK)
        {
            g_CanHal_Error = false;
        } 
    }
}

int32_t GET_CAN_Stat_Err_Count(void)
{
    if (HAL_CAN_ERROR_NONE != HAL_CAN_GetError(&CanHandle))
    {
        ++g_can_err_stat_count;
    }
    else 
    {
        --g_can_err_stat_count;
    }
    if (0>g_can_err_stat_count) g_can_err_stat_count = 0;
    SET_DEBUG_Transmit_IT((uint8_t *)&g_can_err_stat_count, 1);
    
    return g_can_err_stat_count;
}
/**
  * @brief  Transmission  complete callback in non blocking mode
  * @param  CanHandle: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  */
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *CanHandle)
{

#if 0   //test code 
    //SET_DEBUG_Transmit_IT((uint8_t *)CanHandle->pRxMsg->StdId, 8);

    SET_DEBUG_Transmit_IT(CanHandle->pRxMsg->Data, 8);
    
    SET_CAN_Receive_IT(CanHandle, CAN_FIFO0);
#endif
    
    can_rx_irq_frame.can_id  = CanHandle->pRxMsg->StdId;
    can_rx_irq_frame.can_ide = CanHandle->pRxMsg->IDE;
    can_rx_irq_frame.can_dlc = CanHandle->pRxMsg->DLC;
    memcpy(can_rx_irq_frame.data, CanHandle->pRxMsg->Data, 8);
    
    g_can_rx_irq_count++;
    g_CanRxMeg_Ready = true;
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
    uint32_t    err_delay = 0x5ffff;
    can_err_irq_count++;
    g_CanRxMeg_Ready = false;
    g_CanHal_Error = true;

    while(--err_delay);
    if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) == HAL_OK)
    {
        /* Reception Error */
        //Can_Error_Handler();
        g_CanHal_Error = false;
    }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Can_Error_Handler(void)
{
    NVIC_SystemReset();
}