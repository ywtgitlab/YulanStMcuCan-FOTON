#include "user_hal_common.h"

static void UartDebug_Error_Handler(void);
static void UartData_Error_Handler(void);

UART_HandleTypeDef UartDataHandle={
      .Instance        = USART2x,
      .Init.BaudRate   = 115200,
      .Init.WordLength = UART_WORDLENGTH_8B,
      .Init.StopBits   = UART_STOPBITS_1,
      .Init.Parity     = UART_PARITY_NONE,
      .Init.HwFlowCtl  = UART_HWCONTROL_NONE,
      .Init.Mode       = UART_MODE_TX_RX,
      .AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT,
};

UART_HandleTypeDef UartDebugHandle={
      .Instance        = USART1x,
      .Init.BaudRate   = 115200,
      .Init.WordLength = UART_WORDLENGTH_8B,
      .Init.StopBits   = UART_STOPBITS_1,
      .Init.Parity     = UART_PARITY_NONE,
      .Init.HwFlowCtl  = UART_HWCONTROL_NONE,
      .Init.Mode       = UART_MODE_TX_RX,
      .AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT,
};

uint8_t g_UartDataRxIrqFrame[32];
uint8_t g_UartDebugRxIrqFrame[32];

__IO ITStatus Uart1RxReady = RESET;
__IO ITStatus Uart2RxReady = RESET;
__IO ITStatus Uart1TxReady = RESET;
__IO ITStatus Uart2TxReady = RESET;


void UART_DebugInit(uint32_t baudrate)
{
      UartDebugHandle.Init.BaudRate = baudrate;

      if(HAL_UART_DeInit(&UartDebugHandle) != HAL_OK)
      {
        UartDebug_Error_Handler();
      }  
      if(HAL_UART_Init(&UartDebugHandle) != HAL_OK)
      {
        UartDebug_Error_Handler();
      }
}

void UART_DataInit(uint32_t baudrate)
{
      UartDataHandle.Init.BaudRate = baudrate;
    
      if(HAL_UART_DeInit(&UartDataHandle) != HAL_OK)
      {
        UartData_Error_Handler();
      }
      if(HAL_UART_Init(&UartDataHandle) != HAL_OK)
      {
        UartData_Error_Handler();
      }
}

/* uart data send */
HAL_StatusTypeDef SET_DEBUG_Transmit_IT(uint8_t *pData, uint16_t Size)
{
    if(HAL_UART_Transmit_IT(&UartDebugHandle, pData, Size)!= HAL_OK)
    {
        UartDebug_Error_Handler();
    }
    return HAL_OK;
}

HAL_StatusTypeDef SET_DATE_Transmit_IT(uint8_t *pData, uint16_t Size)
{
    if(HAL_UART_Transmit_IT(&UartDataHandle, pData, Size)!= HAL_OK)
    {
        UartData_Error_Handler();
    }
    return HAL_OK;
}

/* uart data recv */
HAL_StatusTypeDef SET_DATE_Receive_IT(uint8_t *pData, uint16_t Size)
{
    if(HAL_UART_Receive_IT(&UartDataHandle, pData, Size) != HAL_OK)
    {
        UartData_Error_Handler();
    }
    return HAL_OK;
}

//      /* The board sends the message and expects to receive it back */
//      
//      /*##-2- Start the transmission process #####################################*/  
//      /* While the UART in reception process, user can transmit data through 
//         "aTxBuffer" buffer */
//
//      
//      /*##-3- Wait for the end of the transfer ###################################*/   
//      while (Uart1TxReady != SET)
//      {
//      }
//      
//      /* Reset transmission flag */
//      Uart1TxReady = RESET;
//      
//      /*##-4- Put UART peripheral in reception process ###########################*/  
//      if(HAL_UART_Receive_IT(&Uart1Handle, (uint8_t *)aRxBuffer, 30) != HAL_OK)
//      {
//        Error_Handler();
//      }


/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of IT Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: transfer complete */
  if (UartHandle->Instance == USART1x) {
      Uart1TxReady = SET;
  }else if (UartHandle->Instance == USART2x) {
      Uart2TxReady = SET;
  }
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: transfer complete */
  if (UartHandle->Instance == USART1x) {
      Uart1RxReady = SET;
  }else if (UartHandle->Instance == USART2x) {
      Uart2RxReady = SET;
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void UartData_Error_Handler(void)
{
  NVIC_SystemReset();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void UartDebug_Error_Handler(void)
{
    NVIC_SystemReset();
}