/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal_can.h"
#include "appCommon.h"    
#include "user_hal_common.h"
#include "foton-can.h"

#include <string.h>
#include <stdbool.h>
/** @addtogroup STM32F0xx_HAL_Examples
  * @{
  */

/** @addtogroup UART_TwoBoards_ComIT
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

static void main_loop(void);
//static void Error_Handler(void);

extern CAN_FrameTypeDef     can_rx_irq_frame;
//extern CAN_FrameTypeDef     frame;
extern CAN_HandleTypeDef    CanHandle;

/* User variable*/
//SYS_VariateDef	            sysVariate;
extern volatile bool    g_CanRxMeg_Ready;
extern volatile bool    g_CanHal_Error;
extern volatile uint8_t g_need_check_can;
extern volatile uint32_t can_err_irq_count;

static void main_loop(void)
{
    uint32_t    timeStart = HAL_GetTick();
    
    while(1){
        
        if((HAL_GetTick() - timeStart) >= 200) {
            IWDG_Refresh();
            
            if (1 == g_need_check_can) CHECK_CAN_RxItCount();
            
            //if (true == g_CanHal_Error) SET_CAN_Force_Rx_IT();   
            
            if (2000 <= can_err_irq_count) NVIC_SystemReset();
            
            //if (10 < GET_CAN_Stat_Err_Count()) NVIC_SystemReset();
            
            timeStart = HAL_GetTick();
        }
    
        CAN_CycleMegTx();
        
        if (true == g_CanRxMeg_Ready){
            CAN_RxMegAnalysis(&can_rx_irq_frame);
            g_CanRxMeg_Ready = false;
            SET_CAN_Receive_IT(&CanHandle, CAN_FIFO0);
        }
    }
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
      HAL_Init();
      /* Configure the system clock to 48 MHz */
      SystemClock_Config();
      /* Configure LED3 */
      BSP_LED_Init(LED3);

      IWDG_Init();
     
      BSP_LED_On(LED3);

      UART_DebugInit(115200);
      UART_DataInit(115200);
      
      CAN_Init(500);
      
      SET_DEBUG_Transmit_IT("Hardware Ready\r\n", strlen("Hardware Ready\r\n"));
      /* Infinite loop */
      main_loop();
}

void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  
  /* No HSE Oscillator on Nucleo, Activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_NONE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
}




#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif


/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
