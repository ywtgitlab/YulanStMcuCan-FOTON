#ifndef _FOTON_CAN_H_
#define _FOTON_CAN_H_

#include <stdint.h>

#define CAN_ID_BRAKE1       0x130
#define CAN_ID_SAS1         0x03a
#define CAN_ID_BCM5		    0x360
#define CAN_ID_ICM1         0x470
#define CAN_ID_AUDIO2	    0x344

enum AVM1_ST_AVM_MODE{
    ST_AVM_MODE_INACTIVE,
    ST_AVM_MODE_2D,
    ST_AVM_MODE_3D
};

enum AVM1_ST_VIEWIND{
    ST_VIEWIND_FRONT = 0x01,
    ST_VIEWIND_REAR,
    ST_VIEWIND_LEFT,
    ST_VIEWIND_RIGHT,
    ST_VIEWIND_3D_BASE = 0x04,
};

enum AVM1_ST_AVMON{
    ST_ON_AVM_OFF,
    ST_ON_AVM_STATING,
    ST_ON_AVM_NORMAL,
};

enum AVM1_ST_WARN_AVM{
    ST_WARN_NONE,
    ST_WARN_AVM_ERR,
    ST_WARN_CAMERA_ERR,
    ST_WARN_SPEED_UP15KM,
    ST_WARN_SPEED_DN15KM_OPENERR
};

typedef struct sas1_msg{
    uint8_t  dummyB0[4];
    uint8_t  dummyb0:1;
    uint16_t EPS3_N_CSWA:15;
    uint8_t  EPS3_St_CSWASign:1;
    uint8_t  dummyb1:7;
    uint8_t  dummyB1[1];
}SAS1Msg_t;

typedef struct bcm5_msg{
    uint8_t dummyB0[2];
    uint8_t dummyb0:5;
    uint8_t LA1_St_LowBeam:1;
    uint8_t LA1_St_HighBeam:1;
    uint8_t dummyb1:1;
    
    uint8_t dummyb2:1;
    uint8_t LA1_St_DirectionLampL:1;
    uint8_t LA1_St_DirectionLampR:1;
    uint8_t dummyb3:2;
    uint8_t LA1_S_DirectionLampL:1;
    uint8_t LA1_S_DirectionLampR:1;
    uint8_t dummyb4:1;  
    
    uint8_t dummyB1[4];
}Bcm5Msg_t;

typedef struct icm1_msg{
    uint8_t dummyB0[6];
    uint8_t ICM1_N_ReverseGear:1;
    uint8_t dummyb0:2;  
    uint8_t ICM1_N_Handbrake:1;
    uint8_t dummyB1[1];
}Icm1Msg_t;

typedef struct audio2_msg{
    uint8_t dummyB0[3];
    uint8_t AUDIO2_S_AVM:2;
    uint8_t AUDIO2_S_AVM_Mode:2;
    uint8_t AUDIO2_St_AVMViewSel:3;
    uint8_t dummyb0:1;
    uint8_t dummyB1[1];
    uint8_t AUDIO2_S_Brightnesscontrol:2;
    uint8_t dummyb1:6;
    uint8_t dummyB2[2];
}Audio2Msg_t;

typedef struct cycle_msg{
//    uint8_t dummy0:4;
//    uint8_t AVM1_Checksum:3;
//    uint8_t dummy1:1;
//    uint8_t AVM1_RollingCounter:2;
//    uint8_t dummy2:2;
    uint8_t AVM1_St_AVM_Mode:3;
    uint8_t AVM1_St_Calibration:4;
    uint8_t dummy0:1;
    uint8_t AVM1_St_ViewInd:3;
    uint8_t dummy1:1;
    uint8_t AVM1_St_AVMOn:4;
    uint8_t AVM1_St_Warn_AVM:1;
    uint8_t AVM1_F_FrontCamera:1;
    uint8_t AVM1_F_RearCamera:1;
    uint8_t AVM1_F_LeftCamera:1;
    uint8_t AVM1_F_RightCamera:1;
    uint8_t dummy2:4;
    uint8_t dummyB[5];
}CycleMsg_t;



void CAN_RxMegAnalysis(CAN_FrameTypeDef *can_frame);
void CAN_CycleMegTx(void);
void Foton_CanMegTx(CycleMsg_t *data_Frame);

#endif 