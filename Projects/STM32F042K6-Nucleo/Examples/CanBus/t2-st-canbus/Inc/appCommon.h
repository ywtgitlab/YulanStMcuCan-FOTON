 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APP_COMMON_H
#define __APP_COMMON_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
#define DBG_LOG(...)
#define DBG_ERR()

/*
 * Controller Area Network Identifier structure
 *
 * bit 0-28	: CAN identifier (11/29 bit)
 * bit 29	: error frame flag (0 = data frame, 1 = error frame)
 * bit 30	: remote transmission request flag (1 = rtr frame)
 * bit 31	: frame format flag (0 = standard 11 bit, 1 = extended 29 bit)
 */

typedef signed char         __s8;
typedef unsigned char       __u8;
typedef signed short        __s16;
typedef unsigned short      __u16;
typedef signed int          __s32;
typedef unsigned int        __u32;
typedef signed long long    __s64;
typedef unsigned long long  __u64;

typedef unsigned int canid_t;
typedef unsigned char uartid_t;
/* Exported struct ------------------------------------------------------------*/
/**
 * struct can_frame - basic CAN frame structure
 * @can_id:  the CAN ID of the frame and CAN_*_FLAG flags, see above.
 * @can_dlc: the data length field of the CAN frame
 * @data:    the CAN frame payload.
 */
typedef struct can_frame {
	canid_t can_id;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
	__u8 can_ide;
	__u8 can_dlc; /* data length code: 0 .. 8 */
	__u8 data[8]; /* code: 0 .. 8 */
}CAN_FrameTypeDef;

typedef struct uart_frame{
	uartid_t uart_id;
	__u8 uart_rw;
	__u8 uart_dlc;
	__u32 data[8];
	__u8  veify_rcc;
	__u8 start_bit;
	__u8 stop_bit;
}UART_FrameTypeDef;

typedef struct variate_frame{
	__u32 switchViewNum;
    __u32 switchChannelnum;
	__u16 steerAngle; //guide line ang message	
	__u16 sideParkProgBar;// 0 1 2 3  //side parking steering wheel ang message
	__u16 sideParkProgBarYel;// 0 1 2	
    __u16 sideParkExceptional;
    __u16 :0;
	__u16 reverseGear;
	__u8  canConnectState;// 0:NULL  1:init finish 2:keep-alive
	__u8  bcsId;
	__u8  bcsVaule;
	__u8  steerDrection; //0  left  ;   1  right        
}SYS_VariateDef;
/* Exported constants --------------------------------------------------------*/

void CAN_SideParkMegPro(void);
void CAN_CycleMegRes(void);
void CAN_RxMegPro(CAN_FrameTypeDef frame);
void UART_MegTxPro(UART_FrameTypeDef frame);

void APP_CanToUartTx(void);
void APP_CanMegTx(CAN_FrameTypeDef cFrame);
void APP_UartMegTx(unsigned char *txBuff,unsigned int size);

#endif /* __APP_COMMON_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

