#ifndef _USER_HAL_COMMON_H_
#define _USER_HAL_COMMON_H_

#include "main.h"

void IWDG_Init(void);
void IWDG_Refresh(void);

void CAN_Init(uint32_t baudrate_kbps);
void CHECK_CAN_RxItCount(void);
int32_t GET_CAN_Stat_Err_Count(void);
void SET_CAN_Force_Rx_IT(void);
HAL_StatusTypeDef SET_CAN_Receive_IT(CAN_HandleTypeDef* hcan, uint8_t FIFONumber);
HAL_StatusTypeDef SET_CAN_Transmit_IT(CAN_HandleTypeDef *hcan);

void UART_DataInit(uint32_t baudrate);
HAL_StatusTypeDef SET_DATE_Transmit_IT(uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef SET_DATE_Receive_IT(uint8_t *pData, uint16_t Size);

/*
*   Example: Debug info sendto console
*   Code:
*       SET_DEBUG_Transmit_IT("Hardware Ready\r\n", strlen("Hardware Ready\r\n"));
*/
void UART_DebugInit(uint32_t baudrate);
HAL_StatusTypeDef SET_DEBUG_Transmit_IT(uint8_t *pData, uint16_t Size);

#endif