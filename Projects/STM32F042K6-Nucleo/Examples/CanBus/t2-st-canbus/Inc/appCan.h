/**
  ******************************************************************************
  * @file    Templates/Inc/main.h 
  * @author  MCD Application Team
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APP_CAN_H
#define __APP_CAN_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "appCommon.h"
#include "stm32f0xx_hal.h"
/* Exported types ------------------------------------------------------------*/
/* special address description flags for the CAN_ID */
#define CAN_EFF_FLAG 0x80000000U /* EFF/SFF is set in the MSB */
#define CAN_RTR_FLAG 0x40000000U /* remote transmission request */
#define CAN_ERR_FLAG 0x20000000U /* error frame */

/* valid bits in CAN ID for frame formats */
#define CAN_SFF_MASK 0x000007FFU /* standard frame format (SFF) */
#define CAN_EFF_MASK 0x1FFFFFFFU /* extended frame format (EFF) */
#define CAN_ERR_MASK 0x1FFFFFFFU /* omit EFF, RTR, ERR flags */

#define CAN_ID_SVM			0x1E1
#define CAN_ID_VEHICLE_SPEED		0x508
#define CAN_ID_TURN_LIGHTS      	0x320
#define CAN_ID_IVI			0x1E8
#define CAN_ID_GEAR_SELECT_LEVEL 	0x43F
#define CAN_ID_SAS			0x2B0

#define CAN_ID_AV_RES			0x17330B10 //| CAN_EFF_FLAG
#define CAN_ID_AV_STATUS		0x6AF
#define CAN_ID_AV_ACTIVITY_555		0x555
#define CAN_ID_AV_ACTIVITY_17F		0x17F00069 //| CAN_EFF_FLAG
#define CAN_ID_AV_ACTIVITY_1B0		0x1B000069 //| CAN_EFF_FLAG

#define CAN_ID_HMI_REQUEST		0x17330B00
#define CAN_ID_VIEW_REQUEST		0x497
#define CAN_ID_WHEEL_REQUEST		0x9F
#define CAN_ID_HODOMETER_REQUEST	0x116
#define CAN_ID_GETRIEBE_REQUEST		0xAD


#define FRAME_HMIRX_HEAD					0x22
#define FRAME_HMITX_HEAD					0x42
#define FRAME_RESTART_HEAD				0X12

//AREAVIEW = AV
#define FRAME_AV_2D								0xDA
#define FRAME_AV_3D								0xDF
#define FRAME_AV_COLOR						0xD7
#define FRAME_AV_CONTRAST					0xD8
#define FRAME_AV_BRIGHT						0xD9

#define FRAME_CAR_FORWARD					0x0F
#define FRAME_CAR_BACKWARD				    0x5F
#define FRAME_CAR_STOP						0xFF

#define FRAME_CAR_GETRIEBE_P			0x14
#define FRAME_CAR_GETRIEBE_R			0x18
#define FRAME_CAR_GETRIEBE_N			0x1C
#define FRAME_CAR_GETRIEBE_DS			0x20

#define EN_OFF 										0
#define EN_ON  										1

#define SLEEP_MS									1
#define SLEEP_MS25								25
#define SLEEP_MS50								50
#define SLEEP_MS100								100
#define SLEEP_MS250								250
#define SLEEP_MS500								500
#define SLEEP_MS1000							1000


#endif /* __CAN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
